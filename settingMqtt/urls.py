from django.urls import path
from . import views

urlpatterns = [
    path('',views.home),
    path('registerConnection/',views.mqttRegister),
    path('editionConnection/<client_id>',views.editionConnection),
    path('editConnection/',views.editConnection),
    path('deleteConnection/<client_id>',views.deleteConnection),
    path('connectConnection/',views.connectConnection),
]